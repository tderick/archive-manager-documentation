
Diagramme des cas d'utilisation
===============================

Trois acteurs vont interagir dans notre système:

- **Gestionnaire**: C’est le personnage chargé de la gestion du système et de son bon fonctionnement. Il est le tout premier utilisateur de la plateforme.
  
- **membre**: C’est un personnage qui est enregistré dans la base de données. Il est reconnu du système et pourra donc réaliser l’ensemble des actions qui lui auront étés assignées par l’administrateur de la plateforme.
  
- **visiteur**: C’est un personnage non membre du système, il n’est pas enregistré dans la base de données. De ce fait, il ne possède presque aucunes actions réalisables dans le système si ce n’est la consultation des archives enregistrées pour le grand publique.

A ces différents acteurs, un ensemble de cas d’utilisation leur sont affectés. Nous ferons ci-
dessous, pour chaque acteur, dans un 1 er temps la synthèse de ces différents cas d’utilisations
(diagramme de cas d’utilisation) et dans un second temps une description textuelle de ces cas
pour une meilleure compréhension.

.. image:: ../assets/images/diagramme_de_cas_utilisation_visiteur_membre.png
   :alt: Diagramme de cas d'utilisation visiteur/Membre

.. image:: ../assets/images/diagramme_de_cas_utilisation_gestionnaire.png
   :alt: Diagramme de cas d'utilisation Gestionnaire


Description des cas d'utilisation
---------------------------------

Consulter les archives publiques
+++++++++++++++++++++++++++++++++++

Par ce cas, le visiteur aura accès aux archives du groupe public lui seront affiché avec des
filtres (catégorie, date, taille et nom) ; sur chaque archive un bouton pour voir le document en détail et un autre pour télécharger le document. Certaine information ne lui sera pas affiché, à savoir la liste des groupes du document et celui qui à poster le document


Rechercher dans les archives publiques
+++++++++++++++++++++++++++++++++++++++++

La recherche pour les visiteurs se réduit au filtre appliqué au document pendant la consultation.

Télécharger des archives
+++++++++++++++++++++++++++

Pendant la consultation, lorsque l’utilisateur décide de voir en détail un document, il lui est affiché un bouton permettant de télécharger le document. Si c’est dans l’application Desktop alors ce bouton peut être Téléchargé où ouvrir avec le lecteur du système selon que le document est présent dans la base de données locale ou pas. Cette action va donc entraîner la copie de l’archive du serveur vers la machine du client.

Restauration de mot de passe
+++++++++++++++++++++++++++++++

Un membre du système ayant perdu ses coordonnées de connexion et de ce fait se trouvant dans l’état du simple visiteur, il pourra déclencher la procédure de restauration de mot de passe en fournissant l’adresse email de son compte ; un login et un mot de passe seront donc envoyés à l’adresse spécifiée. Il n’aura donc plus qu’à se connecter à son compte email,récupérer ses nouveaux paramètres et à les utiliser.

Exporter vers un support l’ensemble de ses documents
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Il clique sur l’onglet Exporter, dans la nouvelle page qui s’ouvre il choisit les groupes/catégories qu’il souhaite exporter, il aura à choisir le répertoire de destination du fichier résultant. Les fichiers seront alors compresser dans un format zip et irons donc dans le répertoire de destination choisis ou dans le répertoire de téléchargement par défaut s’il est dans l’application web.Pendant l’opération, une barre de progression sera affichée avec un bouton offrant la possibilité d’annuler l’opération, dans l’application Desktop cette barre ne sera pas bloquante, cela signifie qu’il pourra faire d’autres choses avec l’application pendant l’opération.

Verser des archives
++++++++++++++++++++++

L’utilisateur clique sur Ajouter une nouvelle archive, le système lui renvoie la page correspondante, il remplit les métadonnées et référence le fichier de l'archive au biais d’une boite de dialogue lui montrant l’arborescence de son système, il constitue l'ampliation en fonction de ce que lui propose le système, il valide son action et le système effectue les actions suivantes selon la plateforme où il se trouve.

Dans le cas de l’application Desktop, le fichier est enregistré dans la base locale (et donc copié dans l’emplacement local), en suite deux cas de figure :

- S’il est connecté à internet alors le fichier est directement envoyé au serveur ;
- S’il n’est pas connecté à internet alors le système se met attentif à sa prochaine connexion à internet pour envoyer le fichier au serveur.

Déplacer les archives vers la corbeille
++++++++++++++++++++++++++++++++++++++++++

Pendant la consultation, lorsque l’utilisateur décide de supprimer un document,il lui est affiché une boite de dialogue lui demandant de confirmer son action, tout en lui affichantquelques métadonnées pour qu’il sache exactement quel document il est entrain de vouloir supprimer. S’il confirme son action alors le document est n’est pas supprimé dans la base de données globale, le document lui sera désormais invisible tout simplement. Si c’est dans l’application Desktop, le document sera réellement supprimé de sa base de données locale.

Restaurer une archive
++++++++++++++++++++++++

Dans ce cas d’utilisation, l’utilisateur pourra restaurer une archive (la rendre de nouveau visible ou la ramener dans la base de données local dans le cas de l’application Desktop) précédemment supprimer par lui-même.

changer de langue
++++++++++++++++++++

Plus spécifique a l’application mobile l’utilisateur aura la possibilité de choisir la langue à utiliser dans le système notamment entre le français ou l’anglais afin d’être le plus alaise possible au sien de l’application.

Gestion des membres
+++++++++++++++++++++++

La gestion des membres est un cas assez important. Dans ce cas, l’administrateur pourra créer les membres avec leurs données représentatives excepte leur login et mot de passe qui sera générer par l’application elle-même et leur assigner des rôles et groupes de membres qui auront au préalable étés créés. La modification des membres consiste en la modification de leurs données de description, l’édition de ses groupes et de ses rôles, l’activation/désactivation des membres représente leur situation dans le système (un membre bloqué ne pourra rien faire). Pour maintenir la cohérence du système, la suppression des membres est interdite.

Gestion des groupes de membres
++++++++++++++++++++++++++++++++++

Un groupe de membres représente tout simplement un ensemble de membres réunis dans une même entité. L’administrateur doit donc se charger de la gestion de ces groupes à travers les actions de créations, modifications et de suppressions. Pour la création d’un groupe, il suffit juste de lui donner un nom et une description ; ce nom et cette description qui seront modifiés
dans le cas où l’on souhaite éditer le groupe. La suppression consiste simplement à retirer tous les membres appartenant à ce groupe et à l’effacer. L’objectif d’un tel regroupement est de permettre une facilitation de la dissémination dans la mesure où l’ampliation du document, au lieu d’être constituée d’une liste de membre (ce qui risque être pénible et fastidieux si le
nombre de destinataires devient considérable) pourra contenir les différents groupes de membres, et de ce fait, tous les membres des groupes sélectionnés recevront le document comme si on les avait sélectionnés individuellement.

Gestion des rôles
+++++++++++++++++++++

Le système est orienté rôles et de ce fait, les rôles ont donc une place assez importante. Un privilège est une action réalisable dans le système et un rôle est un ensemble de privilèges regroupés. Ainsi, nous allons énumérés l’ensemble des privilèges disponible et les mettre à la disposition de l’administrateur qui se servira donc de ces derniers pour la création des rôles. Dans cette rubrique, nous distinguons 3 actions qui sont la création, la modification et la
suppression. Pour le cas de la création, le nom du rôle, l’ensemble de ses privilèges et la liste des membres associée au rôle (au moins un membre par rôle, il n’existera pas de rôle sans membre) seront demandés. Pour la modification, l’administrateur aura la possibilité de changer le nom, les membres et les privilèges associés au rôle. Pour la suppression, le rôle est
simplement effacé et tous les membres possédant en seront dépossédés.

Gestion des métadonnées
+++++++++++++++++++++++++++

Dans ce système, une métadonnée représente une information de description qui sera associée à une archive, un attribut dont la valeur sera spécifiée à la création de l’archive. Les archives versées dans le système sont attachées à des métadonnées qui vont permettre la réalisation de certaines actions sur ces archives. Pour ne pas avoir un système figé, l’administrateur pourra au travers de ce cas d’utilisation créer et gérer les métadonnées pour les archives ce qui
permettra au système d’être évolutif dans le temps. Il y a des métadonnées de base qui ne pourront pas être modifiées, mais d’autre qui pourront être sujette à des modifications ou suppression.

Gestion des catégories de documents
+++++++++++++++++++++++++++++++++++++++

Une catégorie de documents est un type de documents particulier. Le système d’archivage regroupant des archives de plusieurs types différents, ces types seront donc gérés en tant que catégories de document par des actions de création, édition, activation et désactivation. Une catégorie regroupe un ensemble de métadonnées qui seront demandés à la création de l’archive dans le cas où cette catégorie est sélectionnée. Pour des soucis de cohérence de la
base de données, une catégorie ne pourra pas être supprimée.

Gestion des unités stockages
++++++++++++++++++++++++++++++++

Une unité de stockage est un emplacement ou pourront être stocké les archives (serveurs,disques durs). Ici, l’administrateur pourra définir les unités de stockages comme les serveurs ou encore les disques sur lesquels pourront être réalisés les actions de recopie. Il pourra créer, modifier et supprimer. Un stockage sera défini par son type, son nom, éventuellement son adresse et d’autres informations complémentaires.

Exportation de tout ou une partie des données
+++++++++++++++++++++++++++++++++++++++++++++++++

L’exportation des données est la sauvegarde sur une unité de stockage des archives stockées sur le serveur, il représente le téléchargement d’un zip contenant l’ensemble des archives sélectionnées.

Réplication des données du serveur vers un autre
++++++++++++++++++++++++++++++++++++++++++++++++++++

La réplication des données consiste en un transfert des données du serveur local vers un autre serveur au préalable crée dans les unités de stockages. L’état de la base de données sur le serveur principal est sauvegardé et cet état est transféré sur le serveur de destination et reconstitué sur ce dernier, ensuite, les fichiers archives sont également transférés à l’aide d’une connexion SSH (Secured SHell) établie entre les 2 serveurs. L’exportation des données est la sauvegarde sur une unité de stockage des archives stockées sur le serveur, il représente ;le téléchargement d’un zip contenant l’ensemble des archives sélectionnées.

Consulter la journalisation
+++++++++++++++++++++++++++++++

La journalisation est l’ensemble des actions qui ont étés réalisés dans le système. Par ce cas d’utilisation, l’administrateur pourra savoir ce qui s’est passé dans le système, qui a fait quoi, et pouvoir agir en conséquence si besoin s’impose.

Planification des taches
++++++++++++++++++++++++++++

Une tache planifiée est une tache programmée pour s’exécuter automatiquement à l’instant précisé. L’administrateur pourra créer une tache planifiée en spécifiant son nom et les informations d’exécution parmi lesquelles la tâche qui devra être exécuté ; ces taches seront déjà prédéfinies par défaut à la création du système et son de 2 natures : l’exportation des données et la réplication de ces données (ces actions ont déjà étés explicitées plus haut). Vient ensuite la modification et la suppression de la tache planifiée, premièrement, la modification permettra de modifier les données de la tâche et ensuite la suppression permettra l’effacement de la tâche du système.

