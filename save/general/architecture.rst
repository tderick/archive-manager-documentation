
Architecture physique du système et outils utilisés
===================================================

.. image:: ../assets/images/architecture_systeme.png

Le système suit une architecture REST (Representational State Transfer) avec quelques spécificités, c’est le cas par exemple du type de données renvoyés par le serveur qui sera toujours au format JSON au lieu d’être sous divers formats comme le recommande l’architecture REST. Le choix de cette architecture se justifie par le fait qu’avec une telle architecture, les services communs à l’application web et l’application desktop ne seront pas codés deux fois, chacun aura juste à consommer les services produits par le serveur à sa manière, les réponses du serveur ne seront donc rien que des données brutes.

- Les services back-end du serveur applicatif seront écrit en Java EE car nous avons été familiarisé avec cette technologie le long de notre parcours académique, nous utiliseront un Framework pour réduire le temps du travail et améliorer notre productivité, pour cela celui que nous avons choisi est Spring MVC Framework à cause de sa popularité, sa grande communauté et surtout sa complétude.

- Nous utiliserons une base de données relationnelle, à l’instar de PostgreSQL pour sa robustesse et sa capacité de stockage ; pour la manipulation de cette dernière nous utiliserons une autre technologie Java qui se couple très bien avec Spring MVC Framework et qui facilite grandement la manipulation d’une telle base de données, c’est aussi pour sa grande communauté, il s’agit du fameux Hibernate qui une solution ORM (Object-Relational Mapping) ; son caractère ORM nous facilitera le mapping entre les table de PostgreSQL et les objets Java.

- Notre choix du format JSON (JavaScript Object Notation) comme format de réponse est dû au fait que nous utiliserons des technologies basées sur le JavaScript pour les applications clientes, ainsi la lecture des réponses ne nécessitera aucune conversion de données.

- Pour l’application web nous utiliserons le Framework Angular, c’est le Framework JavaScript le plus répandu pour les applications web, il est particulièrement conçu pour consommer les services REST, ainsi il dispose de nombreux outils pour la communication avec un serveur REST, ce qui se prête très bien à notre objectif.
  

- Pour l’application mobile nous utiliserons le Framework Ionic, c’est l’un des Framework les plus répandus, facile à mettre en œuvre, et à utiliser, il est aussi conçu pour consommer les services REST et offre divers services en vue de bâtir une application mobile robuste.


- Nous utiliserons aussi un Framework pour la manipulation de la base de données locale SQLite, TypeORM pour son rapprochement à la solution Hibernate (solution utilisé coté serveur) à la différence qu’il est en TypeScript basé sur JavaScript.
