FrontEnd documentation
======================

Documentation utilisateur
-------------------------

.. image:: ../assets/images/page_de_connexion.png

Comme nous l'avons vu au niveau des diagrammes des cas d'utilisation, notre application a trois type d'utilisateur.

- **Le gestionnaire**
- **Le membre**
- **Le visiteur**

Nous allons faire une présentation détaillé de chaque fonctionnalité du système en commençant par celle concernant uniquement les gestionnaires, ensuite le membre et enfin le visiteur.

Les actions du gestionnaire
+++++++++++++++++++++++++++
Pour se connecter en tant que gestionnaires, il faut utiliser le login **admin** et le pass **1234**. Bien évidemment ces paramètres seront modifier dans un environement de production.

Une fois connecté en tant que gestionnaire, on se retrouve sur la page d'administration de l'application.

.. image:: ../assets/images/page_administration.png

À partir de cette page, on peut accéde à:

- **La gestion des entités**
- **La gestion des journaux**
- **La gestion du stockage**
- **La gestion du profile**

La gestion des entités
**********************

.. image:: ../assets/images/gestion_entite.png

On a cinq type d'entités:

- Les utilisateurs
- Les roles
- Les groupes
- Les meta données
- Les catégories

Les utilisateurs
%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/liste_utilisateurs.png

Cette page permet de gérer l'ensemble des utilisateurs du système. On peut notamment:

Ajouter un nouveau utilisateur
##############################
Pour ajouter un nouvel utilisateur, il suffit de cliquer sur le bouton **AJOUTER UN UTILISATEUR**. Une fois que vous avez cliquer dessus, vous vous retrouver à la page suivante:

.. image:: ../assets/images/ajout_utilisateur.png

Dans cette page, il vous suffit d'entrer les informations personnelles de l' utilisateur, son login et son mot de passe. Et enfin choisir ses rôles et ses groupes.

Les roles de l'utilisateur vont définir ses droits dans le système. Il peut donc avoir plusieurs rôles et chaque rôles ayant des droits et de permissions qui lui sont associées.

Les groupes de l'utilisateur permettent de définir les documents auxquels il aura accès. Par exemple, si on a un groupe **doyen** et que l'utilisateur n'appartient pas à ce groupe, alors il ne poura pas voir ou être notifier lorsqu'un document sera téléversé dans ce groupe.


Voir les details d'un utilisateur
#################################
On peut voir les détails d'un utilisateur en cliquant sur le bouton **Details** et voir toutes les informations de l'utilisateur.

Mettre à jour  et bloquer l'utilisateur
#######################################

Les boutons **Edition** et **Bloquer** permettent respectivement de mettre à jour les informations d'un utilisateur et bloquer un utilisateur pour l'empêcher de se connecter.

.. warning:: 
    La page de mise à jour des informations de l'utilisateur ne permet pas à l'admin de modifier le login et la mot de passe d'un utilisateur. Seul l'utilisateur lui même peut changer son mot de passe.


Les roles
%%%%%%%%%

.. image:: ../assets/images/liste_roles.png

À partir de cette page, on peut:
- Créer de nouveaux roles
- Voir les details d'un role
- Mettre à jour un role
- Supprimer un role


Créer un role
#############

.. image:: ../assets/images/creation_roles.png

À partir de cette page, on peut créer un role personnalisé, associé des privillèges à ce rôle et attribué ce role à des utilisateurs.

Voir les details, Mettre à jour et supprimer un role
####################################################
Les boutons **Details**, **Editions** et **Supp** permettent respectivement de:

- Voir les details d'un role ainsi que les privillèges associés
- Mettre à jour un role ainsi que les privillèges associés
- Supprimer un role



Les groupes
%%%%%%%%%%%

.. image:: ../assets/images/page_groupes.png

À partir de cette page, on peut créer de nouveaux groupes, les mettre à jour, les Supprimer et y ajouter des utilisateurs



Les meta données
%%%%%%%%%%%%%%%%

.. image:: ../assets/images/list_metadata.png

Une meta donnée est un élément qui permet de décrire un document. Par exemple, un document être décrit par *sa date de publication, l'auteur, etc*. Nous pouvons donc créer nos propres meta données que les membres peuvent utiliser pour décrire les documents qu'ils vont uploader.

Les meta données sont de différents types. On peut avoir des meta données de type: **Texte, Date, Email, Url, Nombre, Time**



Les catégories
%%%%%%%%%%%%%%

.. image:: ../assets/images/liste_category.png

Les catégories permettent de mieux organiser les documents et de mieux gérer les accès à ces documents. Nous pouvons donc créer de nouvelles catégories, les mettre à jour et les bloquer pour empecher les publications dans cette catégorie.


La gestion du stockage
**********************

.. image:: ../assets/images/gestion_stockage.png

Cette section de gestion de stockage nous permet de gérer:

- Les types de fichier accepté par l'application
- Les sites de replications
- L'exportation des données
- La replication


Les types de fichiers
%%%%%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/type_fichier.png

Nous pouvons activer les types de fichiers que notre application va accepter. Après activer ces différents types de fichiers, il faut cliquer sur le bouton **Editer** pour sauvegarder nos préférences.



Sites de replication
%%%%%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/liste_site_replication.png

La replication consiste à dupliquer les données présent sur notre serveur sur d'autres serveurs. Cette section nous permet donc d'enregistrer de potentiel serveur que nous pouvons utiliser pour repliquer nos données. On peut également désactiver les serveurs que nous ne voulons pas qu'ils apparaissent dans la liste des serveurs de replication.  


Replication
%%%%%%%%%%%%

Dans la page de replication, nous pouvons utiliser les serveurs que nous avons enregistrée précédemment pour y repliquer nos données. 

.. image:: ../assets/images/choix_serveur_replication.png

Une fois que nous avons choisi notre serveur de replication, nous pouvons renseigner nos informations de connexion à ce serveur et l'application va y repliquer nos données.

.. image:: ../assets/images/connexion_serveur_replication.png


Le journal
**********

.. image:: ../assets/images/journal.png

Nous consulter l'activité de n'importe quel utilisateur dans le système. Voir les actions qu'il a effectué avec les heures et l'adresse IP à partir de laquelle il a effectué l'opération.

On peut filter son activé par date, par type d'action et par status des opérations effectuées. En gros on peut étudier le comportement de n'importe quel utilisateur du système.

Gestion de son profile
**********************

.. image:: ../assets/images/gestion_profile.png

Ici on peut se deconnecter de sa session ou accéder à la page de son profile. À partir de la, on peut modifier ses informations personnelles, changer sa photo de profile ou mettre à jour son mot de passe.

Les actions du membre
+++++++++++++++++++++


Documentation développeur
-------------------------

Structure des fichiers
++++++++++++++++++++++
Les fichiers du frontend sont organisés comme le montre la figure suivante: 

.. image:: ../assets/images/organisation_fichier_frontend.png 

De haut en bas, on a:

- Le dossier **e2e** qui contient des tests et des fichiers de bout en bout.
- Le dossier **node_modules** qui est est utilisé par le framework angular pour stocker toutes les bibliothèques tierces dont dépendait l'application. Ce dossier est uniquement destiné au développement. Après compilation, certaines de ces bibliothèques sont placées dans un bundle et déployées avec l'application.
- Le dossier **src/app** qui contient le code source réel de l'application. Dans ce dossier, nous avons au moins un module et un composant.
- Le dossier **src/assets** qui contient les actifs statiques de l'application, comme des images, des fichiers texte et des icônes.
- Le dossier **src/environnements** Qui contient les paramètres de configuration pour différents environnements.


Nous explorerons chaque groupe progressivement en commençant par les resources et ensuites le code java.

Le dossier e2e
**************

.. image:: ../assets/images/dossier_e2e.png

ce dossier contient les tests end-2-end de notre application. Il s’agit de tests qui permettent de simuler des parcours
dans notre application du point de vue de l’utilisateur, et s’assurer que tout va bien. Par exemple, « je me connecte », « je modifie mon
identifiant », « je me déconnecte », etc.

Le dossier **src**
******************

Ce dossier contient 03 fichiers:

- **app.e2e-spec.ts**  qui contient un seul test. Il navigue essentiellement vers http://localhost:4200, Afin d'exécuter la suite de tests de bout en bout, on doit exécuter la commande **ng serve**

- **app.po.ts** c est l'objet de la page, c'est l'endroit où nous avons écris les codes pour terminer les éléments de notre page ou vue. Ainsi, dans le futur, si nous souhaitons changer les sélecteurs de notre projet,nos modifications auront un impact seulement dans cet endroit, de sorte que nous n'ayons pas besoin de changer quoi que ce soit dans nos tests.

- **tsconEg.e2e.json**  c'est le fichier de configuation 

Le dossier node_modules
***********************

.. image:: ../assets/images/dossier_node.png

il s’agit du dossier qui contient toutes les dépendances dont notre application a besoin pour fonctionner. notre framework angular a déjà installé ces
dépendances pour nous.
 
Le dossier src
**************

.. image:: ../assets/images/dossier_src.png

C’est dans ce dossier que se trouve l’essentiel du code source de notre projet. C’est dans ce dossier que nous avons
passer la plupart de notre temps en tant que développeur. Tous nos composants Angular, nos templates, nos styles, et tous les autres
éléments de notre application se trouvent dans ce dossier. Tous les fichiers en dehors de ce dossier sont destinés à soutenir la création
de notre application. Vous retrouvez dans ce dossier plusieurs éléments :

- Le dossier **src/app**
- Le dossier **src/assets** 
- Le dossier **src/environnements**

Le dossier **src/app**
%%%%%%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/dossier_srcapp.png

C'est le dossier qui contient le code source de notre application, comme mentionné ci-dessus.On retrouve dans ce dossiers notre premier composant Angular, *app.component.ts* et notre premier module *app.module.ts*
Dans ces deux fragment de code nous présentons notre premier composant Angular  *app.component.ts* et notre premier module *app.module.ts*

.. code-block:: shell

  import { Component, OnInit, OnDestroy } from '@angular/core';
  import { AuthenticationService } from './services/authentication.service';
  import { StorageService } from './services/storage.service';
  import { ArchiveService } from './services/archive.service';
  import { LoadResourceService } from './services/load-resource.service';
  import { Utils } from "./app-utils";
  import { Archive } from './models/archive.model';
  import { HttpEvent, HttpEventType } from '@angular/common/http';
  import { Subscription } from 'rxjs';
  import { Router, NavigationEnd } from '@angular/router';
  
  @Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"]
  })
  export class AppComponent implements OnInit, OnDestroy {
	groups: any[] = [];
	servers: any;
	display = false;
	percentDone: number = 0;
	kbLoaded: string = "";
	kbNeeded: string = "";
	serverId: number;
	serverLogin: string;
	serverPassword: string;
	private param: string[] = [];
	showNavBarAndFooter = false;
	private sub: Subscription;
	constructor(
		private authService: AuthenticationService,
		private loadResourceService: LoadResourceService,
		private storageService: StorageService,
		private archiveService: ArchiveService,
		private router: Router
	) 

pour notre module

.. code-block:: shell

   import { BrowserModule } from '@angular/platform-browser';
   import { NgModule } from '@angular/core';
   import { FormsModule, ReactiveFormsModule } from "@angular/forms";
   import { RouterModule, Routes } from "@angular/router";

   import { UserService } from "./services/user.service";
   import { DataTableFilterService } from "./services/dataTableFilter.service";
   import { AuthenticationService } from "./services/authentication.service";
   import { AlertService } from "./services/alert.service";
   import { AdminGuard } from "./guard/admin.guard";

   import { AppComponent } from "./app.component";
   import { NavBarComponent } from './nav-bar/nav-bar.component';
   import { FooterComponent } from './footer/footer.component';
   import { HomeComponent } from './home/home.component';
   import { Home2Component } from './home2/home2.component';
   import { LoginComponent } from './login/login.component';
   import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
   import { ErrorInterceptor } from './helpers/errorInterceptor.helper';
   import { AddTokenInterceptor } from './helpers/add-token-interceptor.helper';
   import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
   import { EmpliationComponent } from './empliation/empliation.component';
   import { DataTableModule } from "angular-6-datatable";
   import { NotifierModule, NotifierOptions } from "angular-notifier";
   import { EditGroupComponent } from './edit-group/edit-group.component';
   import { AutocompleteLibModule } from 'angular-ng-autocomplete';

    const appRoutes: Routes = [
	{ path: "", component: LoginComponent },
	{ path: "login", component: LoginComponent },
	{ path: "logout", component: LogoutComponent },
	{ path: "forgot-password", component: ForgotPasswordComponent },
	{
		path: "home",
		component: HomeComponent,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 1 }
	},
	{
		path: "home2",
		component: Home2Component,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 1 }
	},
	{
		path: "user-profil",
		component: UserProfilComponent,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 2 }
	},
	{
		path: "group",
		component: GroupComponent,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 11 }
	},
	{
		path: "category",
		component: ShowCategoryComponent,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 11 }
	},
	{
		path: "trash",
		component: TrashComponent,
		canActivate: [PrivilegeGuard],
		data: { privilegeID: 16 }
	},
	{


Le dossier **src / assets**
%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/dossier_assets.png

Les assets constituent toutes les ressources nécessaires au fonctionnement de notre projet developpé avec Angular qui ne sont 
pas des fichiers de code.C’est le dossier qui contient les images de notre application. Dans ce dossier on peut également ajouter d’autres 
fichiers dont nous pouvoir avoir besoin. Un fichier PDF à télécharger pour nos utilisateurs par exemple, des vidéos, ou encore musiques 
nécessaires pour  l'affichage de notre application. 

Le dossier **src/environments**
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. image:: ../assets/images/dossier_environnemnt.png

ce dossier contient un fichier de configuration pour chacun de nos environnements de destination : développement, production, etc. 
on peut y définir des variables d’environnements pour chaque environnement, comme une URL de destination pour nos appels HTTP, qui sera
certainement différente entre notre machine de développement et le serveur de production. Les fichiers seront remplacés à la volée lorsqu’on
générera une archive de notre application.

.. code-block:: shell

  // This file can be replaced during build by using the `fileReplacements` array.
  // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
  // The list of file replacements can be found in `angular.json`.

  export const environment = {
  production: false
  };

  /*
  * For easier debugging in development mode, you can import the following file
  * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
  *
  * This import should be commented out in production mode because it will have a negative impact
  * on performance if an error is thrown.
  */
  // import 'zone.js/dist/zone-error';  // Included with Angular CLI.


Le fichier **browserlist**
%%%%%%%%%%%%%%%%%%%%%%%%%%

C’est un fichier de configuration utilisé par Angular pour paramétrer certains outils en fonction de tel ou tel navigateur. Il permet de déterminer les navigateurs à prendre en charge avec le préfixage.


.. code-block:: shell
  
  # This file is currently used by autoprefixer to adjust CSS to support the below specified browsers
  # For additional information regarding the format and rule options, please see:
  # https://github.com/browserslist/browserslist#queries
  #
  # For IE 9-11 support, please remove 'not' from the last line of the file and adjust as needed

  > 0.5%
  last 2 versions
  Firefox ESR
  not dead
  not IE 9-11

Le fichier **favicon.ico**
%%%%%%%%%%%%%%%%%%%%%%%%%%

Il s’agit de la fameuse icône qui s’affiche dans l’onglet de votre navigateur lorsque vous lancez notre application. Par défaut, il s’agit du logo d’Angular.

.. image:: ../assets/images/dossier_favicon.png



Le fichier **index.html**
%%%%%%%%%%%%%%%%%%%%%%%%%

C’est l’unique page HTML de notre application ! C’est elle qui contient l’ensemble de notre application.

.. code-block:: shell
 
   <!doctype html>
   <!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
   <!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
   <!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
   <!--[if gt IE 8]><!-->
   <html class="no-js" lang="">
   <!--<![endif]-->

	 <head>
   <!-- 		<meta http-equiv="Content-Security-Policy" content="default-src default-src 'self' uds-archivemanger.herokuapp.com; connect-src 'self';font-src 'self'; img-src 'self' data: https:; style-src 'self' ; script-src 'self' 'unsafe-inline' 'unsafe-eval'">
    -->		<meta charset="utf-8">
		      <meta http-equiv="X-UA-Compatible" content="IE=edge">
		      <title>Archives Manager</title>
		      <base href="/">
		      <meta name="description" content="">
		      <meta name="viewport" content="width=device-width, initial-scale=1">
		      <link rel="icon" type="image/x-icon" href="favicon.ico">
		    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"> -->

	     	<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
		    	crossorigin="anonymous"> -->
	  	<script>
			  var global = global || window;
		   	var Buffer = Buffer || [];
		   	var process = process || {
			   	env: { DEBUG: undefined },
				  version: []
		  	};
	  	</script>
	 </head>

   <body>
	    <!--[if lt IE 8]>
		   	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	    <![endif]-->
      	<app-root></app-root>
    </body>

  </html>


Le fichier **karma.conf.js**
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C’est le fichier de configuration de Karma, qui est un outil permettant d’exécuter des tests unitaires dans notre application.

.. code-block:: shell

  // Karma configuration file, see link for more information
  // https://karma-runner.github.io/1.0/config/configuration-file.html

   module.exports = function (config) {
    config.set({
      basePath: '',
      frameworks: ['jasmine', '@angular-devkit/build-angular'],
      plugins: [
        require('karma-jasmine'),
        require('karma-chrome-launcher'),
        require('karma-jasmine-html-reporter'),
        require('karma-coverage-istanbul-reporter'),
        require('@angular-devkit/build-angular/plugins/karma')
      ],
      client: {
         clearContext: false // leave Jasmine Spec Runner output visible in browser
      },
       coverageIstanbulReporter: {
       dir: require('path').join(__dirname, '../coverage'),
       reports: ['html', 'lcovonly'],
       fixWebpackSourcePaths: true
   },
      reporters: ['progress', 'kjhtml'],
      port: 9876,
      colors: true,
      logLevel: config.LOG_INFO,
      autoWatch: true,
      browsers: ['Chrome'],
      singleRun: false
    });
  };


Le fichier **main.ts**
%%%%%%%%%%%%%%%%%%%%%%

C’est le point d’entrée principale de notre application. Il s’occupe de compiler notre application, et lance le module racine de celle-ci.

.. code-block:: shell

   import { enableProdMode } from '@angular/core';
   import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

   import { AppModule } from './app/app.module';
   import { environment } from './environments/environment';

   if (environment.production) {
     enableProdMode();
   }

   platformBrowserDynamic().bootstrapModule(AppModule)
     .catch(err => console.error(err));



Le fichier **polyfill.ts**
%%%%%%%%%%%%%%%%%%%%%%%%%%

Les différents navigateurs existants n’ont pas le même niveau de prise en charge des normes du Web. Les polyfills servent à lisser ces différences en uniformisant le comportement de notre code entre tous les
navigateurs.

.. code-block:: shell

 /*
 * in IE/Edge developer tools, the addEventListener will also be wrapped by zone.js
 * with the following flag, it will bypass `zone.js` patch for IE/Edge
 */
 // (window as any).__Zone_enable_cross_context_check = true;

 /***************************************************************************************************
 * Zone JS is required by default for Angular itself.
 */
 import 'zone.js/dist/zone';  // Included with Angular CLI.


 /***************************************************************************************************
 * APPLICATION IMPORTS
 */
 (window as any).global = window;

Le fichier **style.scss**
%%%%%%%%%%%%%%%%%%%%%%%%%

Ce fichier contient le style global de notre application. 

.. code-block:: shell

  @import "~bootstrap/dist/css/bootstrap.css";


  input{
     text-transform: none !important;
  }

  .backdrop {
    background-color: rgba(0, 0, 0, 0.6);
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
  }

  form input,
  textarea {
	  text-transform: none;
  }

  ng-multiselect-dropdown ul li {
	   list-style-type: none;
  }

  form input,
  textarea {
	  text-transform: none;
  }


Le fichier **test.ts**
%%%%%%%%%%%%%%%%%%%%%%

C'est le point d'entrée principal pour nos tests unitaires.


Le fichier **tsconfig.app.json**
********************************

C'est le fichier de configuration du compilateur TypeScript pour notre application Angular.

.. code-block:: shell

    {
    "extends": "../tsconfig.json",
    "compilerOptions": {
        "outDir": "../out-tsc/app",
        "types": []
    },
    "exclude": [
        "test.ts",
        "**/*.spec.ts"
    ]
    }

Le fichier **tsconfig.spec.json**
*********************************

C'est le fichier de configuration du compilateur TypeScript aussi, mais pour les tests unitaires cette fois.

.. code-block:: shell

 {
   "extends": "../tsconfig.json",
   "compilerOptions": {
    "outDir": "../out-tsc/spec",
    "types": [
      "jasmine",
      "node"
     ]
   },
  "files": [
    "test.ts",
    "polyfills.ts"
  ],
   "include": [
    "**/*.spec.ts",
    "**/*.d.ts"
  ]
 }

Le fichier **tslint.json**
***************************

C’est un fichier de configuration qui définit une syntaxe de code commune à notre projet, et nous aide ainsi à garder un code cohérent.

.. code-block:: shell

 {
    "extends": "../tslint.json",
    "rules": {
        "directive-selector": [
            true,
            "attribute",
            "app",
            "camelCase"
        ],
        "component-selector": [
            true,
            "element",
            "app",
            "kebab-case"
        ]
    }
 }

Le fichier gitignore
*********************

Ce fichier permet de déclarer les fichiers qui ne doivent pas etre commités sur le repository Git.

.. code-block:: shell

 /node_modules
 /dist

Le fichier angular.json
***********************

C'est le fichier de paramétrage central utilisé par Angular; Ce fichier permet de définir ou sont placées les sources de l'application, les scripts js et css tiers... Ce fichier est largement utilisé par la librairie webpack nouvellement ajoutée à Angular.

Le fichier package-lock.json
****************************

Il s’agit d’un fichier propre au fonctionnement du *Node Package Manager*, qui a à voir avec les dépendances de nos dépendances. En effet,
nous avons déclarez nos dépendances dans le fichier package.json, mais elles peuvent elles-mêmes avoir des dépendances qui sont en conflit. 

Le fichier package.json
***********************

C'est le fichier bien connu qui nous a permi  de déclarer toutes les dépendances de notre projet. Egalement d'ajouter nos propres scripts personnalisés, en plus de ceux d’Angular.


Le fichier README.md
********************

C'est le  fichier dans lequel nous avons renseigner les informations pour démarrer et développer notre projet. La  modification de ce fichier, va permettre d’indiquer à toute personne récupérant notre projet comment démarrer
l’application sur sa machine !

Le fichier tsconfig.json
************************
Ce fichier est le fichier de configuration de TypeScript. Il est placé à la racine du  projet également, comme pour le fichier package.json. Nous avons définir un certain nombre d’éléments de
configuration dans ce fichier comme l'illustre le code suivant:



Le fichier tslint.json
**********************

C’est un fichier de configuration qui définit une syntaxe de code commune à notre projet, et nous aide ainsi à garder un code cohérent. Utilisée
lors de l'exécution de **ng lint**. *Linting* aide à garder votre style de code cohérent. De même, c’est le fichier principal de configuration, l’autre fichier mentioné plus haut ne fait qu’étendre.

