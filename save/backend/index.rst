Backend documentation
=====================

Structure des fichiers
----------------------
Les fichiers du backend sont organisés comme le montre la figure suivante:

.. image:: ../assets/images/organisation_fichier_backend.png

De haut en bas, on a:

- Le dossier **src/main/java** qui contient toutes les packages et les classes du projets
- Le dossier **src/main/resources** qui contient les fichiers resources tels que les fichiers de propriétésont
- Le fichier **pom.xml** qui est un fichier géré par Maven et qui contient toutes les dépendances utilisé dans ce projet et leur version 

Nous explorerons chaque groupe progressivement en commençant par les resources et ensuites le code java.


Le dossier src/main/resources
-----------------------------

.. image:: ../assets/images/dossier_resource.png

Le dossier **QRCode**
+++++++++++++++++++++
Ce dossier contient l'image du qrcode qui est utilisé pour apposer un qrcode sur chaque première page d'un document.

Le dossier **save_db**
++++++++++++++++++++++
Ce dossier contient le script shell **saveproddb.sh** qui permet de créer une sauvegarde de la base de données.

.. code-block:: shell

    #!/bin/sh
    #save archive manager data base

    # positionne toi sur le repertoire courant
    # puis si le repertoire `backup` n'existe pas cree le et positionne toi dessus 
    cd `pwd`
    ls
    if [ ! -e backup ]
    then
        mkdir backup
    fi
    cd backup

    # creation de la sauvegarde
    DATE=$(date +%d-%m-%Y-%Hh%M)
    #$DATE=$1
    echo -e "Sauvegarde de la base archivesmanager du ${DATE}"

    mkdir archivesmanager${DATE}

    cd archivesmanager${DATE}

    # dump file
    /usr/bin/pg_dump -h localhost -p 5432 -U postgres -F c -b -w -v -f "archivesmanager-${DATE}.backup" archivesManager

    # SQL file
    /usr/bin/pg_dump -h localhost -p 5432 -U postgres --format plain --verbose  -f "archivesmanager-${DATE}.sql" archivesManager


Dans ce code, les deux dernières commandes retiennent particulièrement notre attention.

.. code-block:: shell

    /usr/bin/pg_dump -h localhost -p 5432 -U postgres -F c -b -w -v -f "archivesmanager-${DATE}.backup" archivesManager

Dans cette commande, les différentes options de la commande **pg_dump** sont:

- **-h** qui permet de spécifier le nom d'hôte de la machine sur laquelle le SGBD est exécuté. Comme le SGBD est la même machine que le l'application, alors l'hôte ici c'est *localhost*. On peut utiliser la version longue de cet argument qui est *--host*
- **-p** precise le port TCP à utiliser pour se connecter au SGBD posgres. Dans notre installation, ce port a été maintenu à 5432. on utiliser *--port** à la place de *-p*
- **-U** (--username) indique le nom d'utilisateur qui veut se connecter au SGBD Postgres. Dans notre cas, c'est le super utilisateur *postgres*
- **-F** (--format) indique le format de sortie de la sauvegarde.Dans notre cas nous voulons avoir une archive personnalisée utilisable par **pg_restore**. Nous allons passer le paramètre **c** à l'option **-F**. Ce paramètre va nous permettre de donner l'extension *.backup* à notre sauvegarde.
- **-b** (--blobs) inclut des objets larges dans la sauvegarde
- **-w** indique à ne jamais demander de mot de passe par authentifier l'utilisateur car pour un script automatique, aucun utilisateur ne sera présent pour taper un mot de passe si cela est demandé. C'est un racourci de l'option **--no-password**. Si pg_dump ne trouve pas automatiquement le mot de passe de l'utilisateur, plutôt que de demander, cette commande va échouer.
- **-v** (--verbose) indique que la commande doit s'effectuer en mode verbeux, autrement dit, pg_dump doit afficher des commentaires détaillés sur les objets et les heures de debut et de fin dans le fichier de sauvegarde.
- **-f** (--file) indique le fichier de sauvegarde

.. note:: 
    **pg_restore** est un outil pour restaurer une base de données PostgreSQL à partir d'une archive créée par **pg_dump** dans un des formats non textuel. Il lance les commandes nécessaires pour reconstruire la base de données dans l'état où elle était au moment de sa sauvegarde


.. code-block::

    /usr/bin/pg_dump -h localhost -p 5432 -U postgres --format plain --verbose  -f "archivesmanager-${DATE}.sql" archivesManager

Cette seconde commande permet de sauvegarder la même base base de données mais cette fois ci en SQL. Dans ce cas, on utilise beaucoup moins d'options.

Le dossier **uploads**
++++++++++++++++++++++
Le dossier **uploads** est celui qui permet de stocker les archives téléversées. Une fois l'archive téléversée, un dossier est crée dans le dossier archive (dans le cas où il n'existe pas encore) ayant le nom de la catégorie de l'archive. Une fois le dossier de la catégorie créer, l'archive y est stocké. Une fois l'archive stocké, un autre dossier est créer dans le dossier **covers** avec toujours le nom de la catégorie où la première page du document téléversé est convertie en image et y est stocké comme image de couvertue.

Le fichier **application.properties**
+++++++++++++++++++++++++++++++++++++
C'est le fichier de configuration par defaut d'un projet Spring Boot. Il est utilisé pour paramétrer le comportement par defaut de l'application.En fonction des dépendances déclarées dans notre projet et en fonction de la valeur des propriétés présentes dans ce fichier, Spring Boot va adapter la création du contexte d’application. Nous allons passer en revue les différents éléments dans ce fichier.

Chargement du driver de connexion à la base de données
******************************************************
.. code-block::

    jdbc.driverClassName=org.postgresql.Driver

Cette ligne indique le driver JDBC à utiliser pour se connecter à la base de données. Comme nous travaillons avec une base de données postgres, le driver utiliser est **org.postgresql.Driver**. Chaque SGBD à utiliser a son drivers. Nous avons par exemple *com.mysql.jdbc.Driver* pour MySQL.


Connexion de l'application à la base de données PostgreSQL
**********************************************************
.. code-block::

    jdbc.url=jdbc:postgresql://localhost:5432/archivesmanager?createDatabaseIfNotExist=true
    jdbc.user=adminuser
    jdbc.pass=un_mot_de_pass_fort
    jdbc.database=archivesmanager
    jdbc.host=localhost
    jdbc.port=5432
    #
    logs.jdbc.url=jdbc:postgresql://localhost:5432/archivesmanager?createDatabaseIfNotExist=true
    logs.jdbc.user=adminuser
    logs.jdbc.pass=un_mot_de_pass_fort

Ici, on a plusieurs options:

- **jdbc.url** indique le chemin de connection à la base de données. Ce lien diffère en fonction du SGBD utilisé. Dans notre cas, la base de données s'appelle **archivesmanager** et est accessible en **localhost** sous le port **5432**. L'option **createDatabaseIfNotExist=true** en fin d'url permet de créer cette base de données si elle n'existe pas encore.
- **jdbc.user** indique l'utilisateur qui souhaite se connecter à cette base de données. Cet utilisateur doit avoir des droits sur cette base de données pour pouvoir y accéder. Sinon le SGBD va refuser l'accès à notre application.
- **jdbc.pass** indique le mot de passe de l'utilisateur qui souhaite se connecter à la BD
- **jdbc.database** indique le nom de notre base de données. Il doit être identique à celui dans l'url
- **jdbc.host** indique le host sur lequel le SGBD est installée. Il doit être le même que celui dans l'url
- **jdbc.port** indique le port de connexion au SGBD. Il doit être le même que celui dans l'url


.. note:: 
    La configuration lié au logs sont pareille que ceux vue précédemment et permette de se connecter au server de logs.

Configuration de Hibernate
**************************

.. code-block::

    spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL9Dialect
    spring.jpa.generate-ddl=false
    hibernate.dialect=org.hibernate.dialect.PostgreSQL9Dialect
    hibernate.show_sql=false
    hibernate.hbm2ddl.auto=update
    hibernate.globally_quoted_identifiers=true
    hibernate.enable_lazy_load_no_trans=true

Hibernate est un framework open source gérant la persistance des objets en base de données relationnelle. Cette section nous permet de configurer notre projet pour utiliser Hibernate comme ORM (Object Relationnal Mapping).

- **spring.jpa.database-platform** permet d'indiquer au moteur de persistance Java quel SGBD nous utilisons pour qu'il sache comment faire le mapping.


Configuration des mails
***********************
.. code-block::

    mail.username=bendes.devteam@gmail.com
    mail.password=bendes@bendes

Ici, on identique les adresses emails à utiliser dans l'application au cas où il faut envoyer des mails

Configuration du CORS (Cross-origin resource sharing )
******************************************************

.. code-block::

    cors.allowedorigin=*

Liste des origines séparées par des virgules à autoriser. '*' autorise toutes les origines. 

Configuration du backup et de l'upload
**************************************
.. code-block::

    backup.dir="/archivesmanager/src/main/resources/save_db/backup/"
    upload.dir="/archivesmanager/src/main/resources/uploads/"
    backup.script.location="/archivesmanager/src/main/resources/save_db/saveproddb.sh"
    ssh.knownhost= file://${user.home}/.ssh/known_hosts

- **backup.dir** indique où les backups automatique vont être sauvegarder
- **upload.dir** indique où les archives uploadés seront sauvegarder
- **backup.script.location** indique où se trouve le script de sauvegarde de la base de données
- **ssh.knownhost** indique la clef SSH à utiliser pour envoyer les sauvegardes automatique sur un serveur distant via le protocol SSH

Configuration du port d'accès de l'application
**********************************************
.. code-block::

    server.port=${PORT:8085}

Cette option permet de changer le port d'accès de l'application. Il suffit juste de le modifier et de mettre celui à utiliser.

Le fichier **log4j.properties**
+++++++++++++++++++++++++++++++
Ce fichier contient la configuration utilisée par la bibliothèque log4j

Le fichier **logsScript.sql**
+++++++++++++++++++++++++++++
Contient le script de création de la table de logs.

Le fichier **script.sql**
+++++++++++++++++++++++++
Contient le script de création de la base de données.





Le dossier src/main/java
-----------------------------

.. image:: ../assets/images/package_explorer.png

Ce dossier contient tout le code du projet regroupé en en 12 packages

Le package bendes.archivesmanager
+++++++++++++++++++++++++++++++++

.. image:: ../assets/images/bendes_archivesmanager.png

Ce package contient juste un seul fichier: le fichier **ArchiveManager.java**. Ce fichier est le point d'entrée du projet Spring car il charge toute la configuration pour nous.

Le package bendes.archivesmanager.bean
++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_beans.png

Ce package contient juste les classes java  et les annotations necessaires pour faire le mapping avec la base de données.

Le package bendes.archivesmanager.config
++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_config.png

Le fichier AbstractAnnotationConfigDispatcherServletInitializer.java
*********************************************************************
Ce fichier permet de configurer le contexte global de notre application en chargeant différentes configuration qui doivent être utiliser.

.. code-block:: java

    @Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { 
				AuthServerOAuth2Config.class,
				OAuth2ResourceServerConfig.class,
				ServerSecurityConfig.class,
				WebSocketConfig.class
			};
	}

En surchargeant la configuration du contexte racine de l'application, nous injectons des configuration de sécurité, de la websocket et celle qui vont nous l'authentification via l'api REST.

Les autres fichiers permettent de modifier le comportement des **servlet**.

Le fichier AuthServerOAuth2Config.java
**************************************
Dans ce fichier, on effectue les configuration liées à l'authentification, à la politique de creation et de révocation de token. Aussi, on connecte cette classe à notre source de données pour qu'elle soit en mesure de gérer l'authentification. 

.. code-block:: java

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() {
    	DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.pass"));
	
        return dataSource;
    }

On utilise l'interface **Environment** pour lire les configurations dans notre fichier **application.properties** et configurer la source de données à utiliser ici.


Le fichier ResourceServerConfig.java
************************************
Dans ce fichier, on configure les politiques d'accès à la route **/api/** pour ne laisser passer que des utilisateur authentifier.

Le fichier ServerSecurityConfig.java
************************************
Ce fichier nous permet de configurer les politique de sécurité en autorisant les accès anonymes à certaines routes. En désactivant certains mode d'authentification comme le basic authentification. Ici on défini aussi l'algorithme à utiliser pour encoder les mots de passse (Dans notre cas **Bcrypt**)

Le fichier WebSocketConfig.java
*******************************
..  code-block::

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/socket")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/new_archive");
        registry.setApplicationDestinationPrefixes("/app");
    }

Ce fichier nous permet de configurer la **websocket** en enregistrant l'endpoint **/socket** accessible par tout le monde et en créant un un borker vers lequel les notifications seront envoyer.


Le package bendes.archivesmanager.controller
++++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_controller.png

Ce package contient toutes les controlleurs de notre applications ainsi que toutes les endpoints défini au niveau de chaque méthode avec des annotations.

La documentation swagger de l'api est accessible via l'adresse **http://localhost:8085/v2/api-docs** dans le cas où l'application est déployé en local sur le port 8085. Le host et le port doivent être réajuster en production pour corresponde aux paramètres que vous aurez utilisé.

.. image:: ../assets/images/api_docs.png


Le fichier ResourceController.java
**********************************
La classe **ResourceController** permet de gérer d'effectuer un ensemble d'opération sur les **covers**, **les archives** et les **avatar** tels que:

- Lire la photo de couverture d'un document
- Télécharger un pdf si on a l'autorisation d'accès à ce pdf
- Lire l'avatar d'un utilisateur connecté
- Mettre à jour l'avatar de l'utilisateur
- Exporter un groupe de PDF en fichier ZIP

Le package bendes.archivesmanager.dao
+++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_dao.png

Ce package contient toutes les classes responsable de l'interaction avec la base de données. La structure des fichier respecte le modèle DAO. Pour modifier un comportement ou faire une recherche particulière dans la base de données qui n'est pas encore implémenté, il faut localiser le bon fichier et y ajouter ce comportement.

Le package bendes.archivesmanager.exception
+++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_exception.png

Ce package permet de regrouper toutes les exceptions personnalisée. Si nous voulons créer une autre execption, il suffit de la créer dans ce package.


Le package bendes.archivesmanager.filter
++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivemanager_filter.png


Le fichier CORSFilter.java
**************************

Ce fichier nous permet de définir les filtres pour le CORS (Cross-origin resource sharing ) et authoriser les autres domaines à accéder à notre serveur. Toutes les requêtes vont passer par ce filtre pour que le serveur détermine si elle va leur accepté ou non.


Le fichier LogFilter.java
**************************
Le filtre de log ici permet de surveiller chaque requête effectuée sur notre système, les catégorisés et créer des logs pour chaque action dans la base de données.


Le package bendes.archivesmanager.listener
++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_listener.png

L'unique class présent dans ce package permet de programmer la tâche de sauvegarde des données de l'application.


Le package bendes.archivesmanager.sauvegarde
++++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivemanager_sauvegarde.png

Ce package contient les classes pour envoyer les notifications par mails et pour faire des sauvegardes respectivement.

Le package bendes.archivesmanager.sendemail
+++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_sendmail.png

Ce package contient une seule classe permettant d'envoyer des mails en utilisant le protocole SMTP.

.. code-block:: java
		
    public static void sendMail(String recepientAdress, String sms, String subject) {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		
		String STAccountEmail = "archivesmanager.uds@gmail.com";
		String STAccountPassW = "#ArchivesManager@2019";
		
		Session session  = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(STAccountEmail, STAccountPassW);
			}
		});
		
		Message message = prepareMessage(session, STAccountEmail, recepientAdress, sms, subject);
		try {
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
 
Pour utliser un autre service SMTP autre que gmail, il faut remplacer **smtp.gmail.com** dans la ligne suivante par celui de votre fournisseur de service SMTP ou celui de votre propre serveur SMTP.

.. code-block:: java

    properties.put("mail.smtp.host", "smtp.gmail.com");

Vous devez ensuite remplacer les valeurs des variables suivantes par vos identifiants SMTP.

.. code-block:: java

    String STAccountEmail = "archivesmanager@gmail.com";
    String STAccountPassW = "#ArchivesManager";


Le package bendes.archivesmanager.service
+++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_service.png

Ce package contient l'ensemble des services de l'application. Les services sont les seules à utiliser les DAO pour communiquer avec la base de données. Pour interagir avec la base de données, il faut utiliser le service adéquat.


Le package bendes.archivesmanager.utils
+++++++++++++++++++++++++++++++++++++++++++

.. image:: ../assets/images/archivesmanager_utils.png

Ce package contient les classes utilitaires de notre application.

Le fichier PdfToImage.java
**************************
Ce fichier permet principalement de :

- Créer l'image de couverture d'un pdf
- Redimensionner une image

Le fichier Plannification.java
******************************
Ce fichier contient le code permettant d'exécuter un ensemble de tâche programmée parmis lesquelles le **backup** de la base de données.


Le fichier QRCodeGenerator.java
*******************************
Cette classe contient le code permettant de manipuler le QRCode sur une image.
