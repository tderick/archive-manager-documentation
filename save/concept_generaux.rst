Archive Manager !
===================

**Archive Manager** est un système d'archivage et de dissémination de documentation administratifs dans le cas de l'université de Dschang. Il présente plusieurs fonctionnalités que sont:

- Importation des archives
- Rangement des archives par groupes d’utilisateur et par catégorie
- Recherche d’une archive
- Suppression et restauration d'une archive
- Imposition d’un QR code sur chaque archive ajoutée
- Suggestion de méta données pour chaque archive importer
- Lecteur de QR-code (L'application mobile)


.. Architecture globale
.. image:: assets/images/architecture_globale.png

Ce système d'archivage est composé de **04 principaux composants** que sont:

- Un backend développé en **Spring Boot** couplé avec une base de données **PosgreSQL**
- Un FrontEnd développé avec **Angular 7**
- Un application mobile développé en **IONIC 5**
- Une application desktop développé avec **ElectronJS**



