Mise à jour Backend
===================

24 juillet 2022
---------------

Ajout du document téléversé en fichier joint du mail de notification du téléversement d'un nouveau document.

.. image:: assets/images/update/fichier_joins_au_mail.png 

|

23 juillet 2022
---------------

**Repartition de la configuration de Spring boot en deux fichiers de configuration:**

- **application-dev.properties** pour les configurations de Spring Boot pour l'environement de dévelopment
- **application-prod.properties** pour les configurations de Spring Boot pour l'environement de production

Le fichier de configuation principal **application.properties** permet d'indiquer quel configuration sera utiliser dans l'environment où on se trouve en renseignant la propriété **spring.profiles.active=** avec les valeurs **prod ou dev**.

*Exemple: spring.profiles.active=prod pour activer la configuration de l'environement de production et spring.profiles.active=dev pour activer la configuration de l'environement de développement*

.. note::
    **dev ou prod** correspondent à la partie du nom apres le **-** du nom du fichierde configuation


.. image:: assets/images/update/application.properties.png 

|

**Ajout de l'url du frontend dans les fichiers application-dev.properties et application-prod.properties** 

Lorsque un document est téléversé dans le système, un QRCode est imposé dessus contenant le lien d'accès web à ce document. Le paramètre **frontend.url=** dans les fichiers de properties permet de spécifier ce lien pour faciliter le changement d'environement.

.. image:: assets/images/update/frontendurl.png 

|

30 avril 2022
--------------
Containeurisation de l'application. Le backend a été containeurisé pour faciliter son deploiement aussi bien en environement de production que de développement. Ainsi, le **docker-compose-dev.yml** est prévue pour lancer le backend en version de développement et le **docker-compose.yml** est prévue pour lancer le backend en version de prod.


19 avril 2022
--------------
Ajout d'une fonctionnalité d'inscription d'un lot d'utilisateur à partir d'un fichier excel. Voir :ref:`Ajouter un groupe d'utilisateurs` pour plus d'explication.