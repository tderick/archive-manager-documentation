Installation
============

Partie Backend
--------------

Les prérequis à installer sur son poste pour pouvoir faire fonctionner le backend sont:

- **Le JDK 1.8 (Java Development Kit)**
- **PosgreSQL**
- **Pgadmin 4**
- **Spring Tool Suite**
- **git**

Etape 1 : Cloner le projet
+++++++++++++++++++++++++++
.. code-block:: shell

    git clone https://gitlab.com/archives-manager/archivemanager-server.git

Etape 2 : importation du projet dans notre IDE Spring Tool Suite
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1- Dans Spring Tool Suite, sélectionnez **File -> Import…**

.. image:: assets/images/file_import.png 

2. Dans la fenêtre d'importation, développez **Maven**, sélectionnez **Existing Maven Projects** et cliquez sur **Next>**

.. image:: assets/images/select_maven_project.png

3. Cliquez sur **Browser...** et sélectionnez le dossier du projet que nous venons de cloner et enfin cliquez sur **Finish**

.. image:: assets/images/select_project.png

Après cette étape, Spring Tool Suite va automatiquement installer les dependances en lisant le fichier **pom.xml**.

.. image:: assets/images/install_dependancy.png

Etape 3 :  Création de la base de données 
++++++++++++++++++++++++++++++++++++++++++

.. note::
    Avant de procéder aux étapes dans cette section, assurez-vous d'avoir connecter votre **pgadmin 4** avec votre instance de postgres.

1. Ouvrir **pgAdmin 4** et afficher le menu contextuel du serveur que vous avez créer puis
sélectionner **Create -> Database..**

.. image:: assets/images/create_db_start.png

2.Saisir le nom de la base de données : **archivesmanager**

3.Choisir le propriétaire de la BDD dans la Combobox Owner: **postgres**

4. Saisir un commentaire : **Base de données pour l’application ArchivesManager**

.. image:: assets/images/creation_db_end.png

Après la saisie des paramètres de création de la base de données, cliquer sur **Save** pour
enregistrer.

Etape 4 :  Exécution du script de création des tables de la base de données 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rendez-vous dans votre IDE Spring Tool suite et parcourir le projet comme le montre la figure suivante afin de récupérer le script SQL de notre base de données :

.. image:: assets/images/script_sql.png

Une fois le script copié, nous allons nous rendre dans pgAdmin 4 afin de coller son contenu et
l’exécuter :

1. Clic droit sur le nom de notre base de données, ensuite sélectionner **Query Tool**.

.. image:: assets/images/query_tool.png

2. Dans le panel de droit qui s'ouvre, coller le script copier précédemment et lancer l’exécution.

.. image:: assets/images/script_execution.png


Notre base de données est maintenant prête. Mais avant de lancer l’exécution de notre Backend, nous devons ici modifier une ligne de la table storages de notre base de données afin de spécifier le répertoire dans lequel seront sauvegarder les archives lors du versement sur notre serveur.

Nous allons ici récupérer le chemin d’accès absolu au répertoire ressources se trouvant dans le dossier du projet. Dans notre cas, ce sera à l’intérieur du projet dans: **/archivemanager-server/src/main/resources**

Naviguez dans **archivesmanager->schemas->public->Tables** pour voir les tables qui ont été créee après l'éxécution de notre script.

.. image:: assets/images/table_list.png

.. image:: assets/images/show_rows.png

Nous allons donc modifier le champs **path** du premier enregistrement se trouvant dans la table **storages**, ceci avec le chemin d’accès copié plus haut.

.. image:: assets/images/modification_storage.png


Etape 5 :  Lancement de notre backend
+++++++++++++++++++++++++++++++++++++

Rendez-vous dans le fichier **application.properties** qui se trouve dans **src/main/resources** et mettez les informations de connexion à votre base de données.

.. image:: assets/images/connect_db.png

L’environnement de développement est maintenant prêt, ainsi que toute les configurations. Pour lancer l’execution du backend de notre application Archives Manager, il faut procéder comme suit:
- Faite un clic droit sur le projet dans le panneau d’exploration
- Sélectionner **Run AS**
- Sélectionner **Spring Boot App**

Après quelques secondes, le backend de notre application sera déployé et sera disponible à
l’adresse : `http://localhost:8080 <http://localhost:8080>`_

.. image:: assets/images/run_app.png

.. image:: assets/images/application_start.png

Partie FrontEnd
---------------

Les prérequis sont:

- **Node v12**
- **Angular CLI**
- **git**

.. note::

    Si vous utilisez Windows, vous devez d'abord procéder aux étapes suivantes:

    1. Installer **Python2** si ce n'est pas installé
    2. Si **Python3** est également déjà installé sur votre système, aller à **System Environment Variables > System Variables > Path** et assurez-vous que les chemins pour Python2 aient la priorité sur ceux de Python3.
    3. Install **Windows Build Tools**
        .. code:: shell

            npm install --global windows-build-tools

    Une fois que tout cela est fait, vous pourrez continuer avec la procédure ci-dessous.


Etape 1 : Cloner le projet
+++++++++++++++++++++++++++

.. code-block:: shell

    git clone https://gitlab.com/archives-manager/archivesmanager-view.git
    cd archivesmanager-view

Etape 2: Desinstallation du package build-angular
+++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    npm uninstall @angular-devkit/build-angular

Etape 3: Desinstallation du package node-sass
+++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    npm uninstall node-sass

Etape 4: Installation de la bonne version de build-angular
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    npm install @angular-devkit/build-angular@0.13.10 --save-dev

Etape 5: Installation des dépendances
++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    npm install

Etape 6: Builder le projet angular
++++++++++++++++++++++++++++++++++

.. code-block:: shell

    ng serve ou npm run build

.. image:: assets/images/build_success.png

Le site web est généré et est accessible à l’adresse http://localhost:4200 Comme le montre la
figure suivante :

.. image:: assets/images/page_login.png

.. note::

    Une fois que vous aurez terminé avec l'étape 5, le resultat de la commande **ng --version** dans le dossier d'archives-manager doit ressembler à ceci:

    .. image:: assets/images/configuration_projet.png

.. warning::
    Le **backend** et le **frontend** étant deux projets complètement indépendant, une fois que le frontend est bien lancé comme montré à la figure précédente, il a besoin de connaître **l'adresse du backend** pour pouvoir se connecter.

    .. image:: assets/images/backendurl.png
    
    |

    Vous devez donc ouvrir le fichier **src/app/app-url.ts** et mettre l'adresse de votre backend au niveau de la constante **SERVER_ADDR** comme indiqué sur la capture précente. L'url qui s'y trouve actuellement (**https://archivesmanager-api.lostpieces.net**) est celui du backend en production. Etant en locale, si vous avez lancé votre backend sur l'adresse **http://localhost:8080**. C'est cette valeur que vous devriez indiquer.

Partie Mobile
-------------

Les prérequis sont:

- **NodeJS**
- **Ionic CLI**
- **Ionic**
- **Capacitor**
- **git**

Etape 1 : Téléchargement des sources du projet
++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    git clone https://gitlab.com/archives-manager/archive-manager-mobile
    cd archive-manager-mobile

Etape 2 : Installation des dépendances du projet
++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

    npm install

Etape 3 : Lancement du projet
+++++++++++++++++++++++++++++

.. code-block:: shell

    ionic serve --lab

Le site web est généré et est accessible à l’adresse http://localhost:8100/ Comme le montre la figure suivante :

.. image:: assets/images/accueil_mobile.png


.. warning::
   Pour que l'application mobile ait accès au backend, vous devez spécifier **l'adresse du backend** dans les fichiers d'environments du projet ionic comme indiqué sur l'image suivante.

    .. image:: assets/images/backendurlmobile.png
    
    |

 