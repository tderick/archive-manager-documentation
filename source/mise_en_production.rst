Mise en production
===================

Pour deployer **ArchivesManager** en production, nous allons utiliser **Docker** et l'architecture globale que nous avons adopté est la suivante:

.. image:: assets/images/architecture_deployment.jpg

Les prérequis pour comprendre cette architecture sont les suivants:

- Avoir les bases sur **Docker**
- Avoir les bases du **docker-compose**

Comme le backend et le frontend sont deux projects complètement indépendant, chacun d'eux est ainsi déployer séparément chacun sous son propre **sous-nom de domaine**. Pour pouvoir rendre les deux projects accessibles sur une même machine, on a utilisé un reverse-proxy qui permet en même temps de sécuriser le traffic vers les applications.

Pour pouvoir lancer les projets en local sur une machine sans avoir besoin de nom de domaine, chacun projet est livré avec un fichier **docker-compose-dev.yml** destiné à tester l'infratructure dans un environment locale.