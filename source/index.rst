.. Archive Manager documentation master file, created by
   sphinx-quickstart on Thu Mar  3 06:23:01 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de Archive Manager !
==================================

**Archive Manager** est un système d'archivage et de dissémination de documentation administratifs dans le cas de l'université de Dschang. Il présente plusieurs fonctionnalités que sont:

- Importation des archives
- Rangement des archives par groupes d’utilisateur et par catégorie
- Recherche d’une archive
- Suppression et restauration d'une archive
- Imposition d’un QR code sur chaque archive ajoutée
- Suggestion de méta données pour chaque archive importer
- Lecteur de QR-code (L'application mobile)


.. Architecture globale
.. image:: assets/images/architecture_globale.png

Ce système d'archivage est composé de **04 principaux composants** que sont:

- Un backend développé en **Spring Boot** couplé avec une base de données **PosgreSQL**
- Un FrontEnd développé avec **Angular 7**
- Un application mobile développé en **IONIC 5**
- Une application desktop développé avec **ElectronJS**


.. toctree::
   :maxdepth: 2

   installation
   guide
   analyse
   documentation_technique
   mise_en_production
   mise_a_jour_backend
   mise_a_jour_frontend
   mise_a_jour_mobile


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
