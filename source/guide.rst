Guide d'utilisation
====================

Partie web
----------

.. image:: assets/images/home_page.png

Comme nous l'avons vu au niveau des diagrammes des cas d'utilisation, notre application a trois type d'utilisateur.

- **Le gestionnaire**
- **Le membre**
- **Le visiteur**

Nous allons faire une présentation détaillé de chaque fonctionnalité du système en commençant par celle concernant uniquement les gestionnaires, ensuite le membre et enfin le visiteur.

Les actions du gestionnaire
+++++++++++++++++++++++++++
Pour se connecter en tant que gestionnaires, il faut utiliser le login **admin** et le pass **1234**. Bien évidemment ces paramètres seront modifier dans un environement de production.

Une fois connecté en tant que gestionnaire, on se retrouve sur la page d'administration de l'application.

.. image:: assets/images/page_administration.png

À partir de cette page, on peut accéde à:

- **La gestion des entités**
- **La gestion des journaux**
- **La gestion du stockage**
- **La gestion du profile**

La gestion des entités
**********************

.. image:: assets/images/gestion_entite.png

On a cinq type d'entités:

- Les utilisateurs
- Les roles
- Les groupes
- Les meta données
- Les catégories

Les utilisateurs
%%%%%%%%%%%%%%%%%

.. image:: assets/images/liste_utilisateurs2.png

Cette page permet de gérer l'ensemble des utilisateurs du système. On peut notamment:

Ajouter un nouveau utilisateur
##############################
Pour ajouter un nouvel utilisateur, il suffit de cliquer sur le bouton **AJOUTER UN UTILISATEUR**. Une fois que vous avez cliquer dessus, vous vous retrouver à la page suivante:

.. image:: assets/images/ajout_utilisateur.png

Dans cette page, il vous suffit d'entrer les informations personnelles de l' utilisateur, son login et son mot de passe. Et enfin choisir ses rôles et ses groupes.

Les roles de l'utilisateur vont définir ses droits dans le système. Il peut donc avoir plusieurs rôles et chaque rôles ayant des droits et de permissions qui lui sont associées.

Les groupes de l'utilisateur permettent de définir les documents auxquels il aura accès. Par exemple, si on a un groupe **doyen** et que l'utilisateur n'appartient pas à ce groupe, alors il ne poura pas voir ou être notifier lorsqu'un document sera téléversé dans ce groupe.


Voir les details d'un utilisateur
#################################
On peut voir les détails d'un utilisateur en cliquant sur le bouton **Details** et voir toutes les informations de l'utilisateur.

Mettre à jour  et bloquer l'utilisateur
#######################################

Les boutons **Edition** et **Bloquer** permettent respectivement de mettre à jour les informations d'un utilisateur et bloquer un utilisateur pour l'empêcher de se connecter.

.. warning:: 
    La page de mise à jour des informations de l'utilisateur ne permet pas à l'admin de modifier le login et la mot de passe d'un utilisateur. Seul l'utilisateur lui même peut changer son mot de passe.

Ajouter un groupe d'utilisateurs
#######################################

.. image:: assets/images/groupe_utilisateurs.png

|

Cette fonctionalité permet d'inscrire un groupe d'utilisateur à la fois à partir d'un fichier Excel. Le fichier Excel d'utilisateurs doit **EXACTEMENT** avoir la structure suivante:

.. image:: assets/images/utilisateurs_lots.png

|

En utilisant l'inscription par lot, la colonne **login** est également utilisée pour le **password**. 

| Les valeurs possible du **grades** sont: **Etudiant, Madame, Monsieur, Docteur, Professeur**. 
| Les valeurs possible du **roles** sont: **Utilisateur,Public**. 
| Les valeurs possible du **groups** sont: **Doyen,Enseignant,Etudiant** 

.. note::
    Vous pouvez ajouter le nombre d'utilisateurs que vous voulez dans le fichier. Cette méthode a été utilisée pour inscrire **330 étudiants d'IN2 de l'année académique 2021-2022 dans le système pour le Beta Testing de l'application.**


Les roles
%%%%%%%%%

.. image:: assets/images/liste_roles.png

À partir de cette page, on peut:
- Créer de nouveaux roles
- Voir les details d'un role
- Mettre à jour un role
- Supprimer un role


Créer un role
#############

.. image:: assets/images/creation_roles.png

À partir de cette page, on peut créer un role personnalisé, associé des privillèges à ce rôle et attribué ce role à des utilisateurs.

Voir les details, Mettre à jour et supprimer un role
####################################################
Les boutons **Details**, **Editions** et **Supp** permettent respectivement de:

- Voir les details d'un role ainsi que les privillèges associés
- Mettre à jour un role ainsi que les privillèges associés
- Supprimer un role



Les groupes
%%%%%%%%%%%

.. image:: assets/images/page_groupes.png

À partir de cette page, on peut créer de nouveaux groupes, les mettre à jour, les Supprimer et y ajouter des utilisateurs



Les meta données
%%%%%%%%%%%%%%%%

.. image:: assets/images/list_metadata.png

Une meta donnée est un élément qui permet de décrire un document. Par exemple, un document peut être décrit par *sa date de publication, l'auteur, etc*. Nous pouvons donc créer nos propres meta données que les membres peuvent utiliser pour décrire les documents qu'ils vont uploader.

Les meta données sont de différents types. On peut avoir des meta données de type: **Texte, Date, Email, Url, Nombre, Time**



Les catégories
%%%%%%%%%%%%%%

.. image:: assets/images/liste_category.png

Les catégories permettent de mieux organiser les documents et de mieux gérer les accès à ces documents. Nous pouvons donc créer de nouvelles catégories, les mettre à jour et les bloquer pour empecher les publications dans cette catégorie.


La gestion du stockage
**********************

.. image:: assets/images/gestion_stockage.png

Cette section de gestion de stockage nous permet de gérer:

- Les types de fichier accepté par l'application
- Les sites de replications
- L'exportation des données
- La replication


Les types de fichiers
%%%%%%%%%%%%%%%%%%%%%

.. image:: assets/images/type_fichier.png

Nous pouvons activer les types de fichiers que notre application va accepter. Après activer ces différents types de fichiers, il faut cliquer sur le bouton **Editer** pour sauvegarder nos préférences.



Sites de replication
%%%%%%%%%%%%%%%%%%%%%

.. image:: assets/images/liste_site_replication.png

La replication consiste à dupliquer les données présent sur notre serveur sur d'autres serveurs. Cette section nous permet donc d'enregistrer de potentiel serveur que nous pouvons utiliser pour repliquer nos données. On peut également désactiver les serveurs que nous ne voulons pas qu'ils apparaissent dans la liste des serveurs de replication.  


Replication
%%%%%%%%%%%%

Dans la page de replication, nous pouvons utiliser les serveurs que nous avons enregistrée précédemment pour y repliquer nos données. 

.. image:: assets/images/choix_serveur_replication.png

Une fois que nous avons choisi notre serveur de replication, nous pouvons renseigner nos informations de connexion à ce serveur et l'application va y repliquer nos données.

.. image:: assets/images/connexion_serveur_replication.png


Le journal
**********

.. image:: assets/images/journal.png

Nous pouvons consulter l'activité de n'importe quel utilisateur dans le système. Voir les actions qu'il a effectué avec les heures et l'adresse IP à partir de laquelle il a effectué l'opération.

On peut filter son activé par date, par type d'action et par status des opérations effectuées. En gros on peut étudier le comportement de n'importe quel utilisateur du système.

Gestion de son profile
**********************

.. image:: assets/images/gestion_profile.png

Ici on peut se deconnecter de sa session ou accéder à la page de son profile. À partir de la, on peut modifier ses informations personnelles, changer sa photo de profile ou mettre à jour son mot de passe.

Les actions du membre
+++++++++++++++++++++
Une fois connecté, on se retrouve sur la page qui liste les catégories des documents déjà archivés et disséminé.

.. image:: assets/images/accueil_utilisateur.png

Versement d’une archive
***********************

- Dans la barre de menu, cliquez sur **‘IMPORTER’**
- Dans la nouvelle interface, cliquez sur le bouton **‘Browser files’**

.. image:: assets/images/televersement_archive.png

- Choisissez un fichier dans la mémoire de votre ordinateur et renseignez le nouveau formulaire qui s’ouvrira en donnant la **catégorie du fichier, le groupe d’utilisateur…** et cliquez par la suite sur le bouton **‘Confirmer’**.

.. image:: assets/images/choisir_fichier.png

- Cliquez ensuite sur le bouton **‘IMPORTER’** et  un message de confirmation s ‘affichera en cas de succès et une notification en rouge sur le groupe de l’archive. Et une notification par email sera envoyé aux intéressé de l’archive.

.. image:: assets/images/importer_archives.png

Visualiser les archives
***********************
- Au niveau de la barre de menu, sur l’onglet **‘ARCHIVES’** choisir le mode visualisation (Par groupe ou par catégorie)
- Nous choisissons le mode par catégorie et nous voyons la toute nos catégories d’archives

.. image:: assets/images/list_archives.png

- En cliquant sur le dossier **‘Fiche de Td’** nous verrons le fichier que nous avons importé précédemment.

.. image:: assets/images/visualisation_archives.png

Recherche d’un document
***********************
- Cliquez sur le bouton **‘RECHERCHER’** au niveau de la barre de menu.
- Dans l’interface qui s’affichera vous verrez un formulaire affiché avec un tableau contenant la liste des archives en base de données.

.. image:: assets/images/recherche_archives.png

- Entrez le nom de l’archive rechercher au niveau du champ **‘Nom’** du formulaire et grâce à l’auto-complétion, le tableau en dessous se mettra à jour chaque fois que vous entrerez un caractère du nom de l’archive en vous proposant les archives possédant se caractère dans leur nom.

.. image:: assets/images/recherche_archive_par_nom.png

- Et par la suite vous pourriez visualiser, téléchargez ou supprimez l’archives trouvez
- Et au cas où l’archive n’existe pas, le tableau retourné sera vide.

Suppression et restauration d’une archive
*****************************************
- Dans la liste des archives présentes, cliquez sur le bouton rouge avec comme icône  une corbeille.

.. image:: assets/images/page_restauration_archive.png

- Puis cliquez sur le bouton **‘Confirmer’** pour supprimer définitivement l’archive et l’archive disparaîtra de la liste de vos archives.

.. image:: assets/images/confirmation_suppression_definitive.png

Pour restaurer une archive qui avait été supprimé :

- En survolant l’onglet portant le nom d’utilisateur sur la barre de menu, cliquez sur **‘Corbeille’** dans le petit menu afficher.

.. image:: assets/images/page_corbeille.png

- L’interface présentée présente la liste des archives que vous avez déjà supprimé. Et en cliquant sur le bouton de couleur orange de l’archive voulu, elle sera restaurée automatiquement.

Partie Mobile
-------------

Une fois connecté, on se retrouve sur la page qui liste les catégories des documents déjà archivés et disséminé.

.. image:: assets/images/accueil_mobile_list_category.png

Versement d’une archive
+++++++++++++++++++++++

- Dans le side menu, cliquez sur **‘AJOUTER UNE ARCHIVE’** en haut, Ou alors sur l’écran d’accueil le bouton **‘PLUS’**

.. image:: assets/images/page_ajout_archives.png

- Dans la nouvelle interface, cliquez sur le bouton **‘CHOOSE FILE’**

.. image:: assets/images/import_file_mobile.png

- Choisissez un fichier dans la mémoire de votre ordinateur et renseignez le nouveau formulaire qui s’ouvrira en donnant **la catégorie du fichier, le groupe d’utilisateur…** et cliquez par la suite sur le bouton **‘VALIDER’**.

.. image:: assets/images/recapitulatif_import.png

- Cliquez  esuite sur le bouton **‘IMPORTER’** et  un message de confirmation s ‘affichera en cas de succès et une notification en rouge sur le groupe de l’archive. Et une notification par email sera envoyé aux intéressé de l’archive.

.. image:: assets/images/confirmation_import_mobile.png

Visualiser les archives
+++++++++++++++++++++++

- Apres l’ajout de l’archive, l’utilisateur versant ainsi que tous les autres utilisateurs sont notifiés de l’ajout de ladite archive. Pour y accéder, on peut soit cliquer sur la cloche de notification à l’accueil au niveau du segment, soit se rendre dans le Side-Menu et cliquer sur **‘NOTIFICATION’**

.. image:: assets/images/page_notification_mobile.png

- Ceci fait, nous nous retrouvons dans la vue de toutes les notifications :

.. image:: assets/images/list_notification.png

- Il ne suffit plus que de sélectionner le document que l’on souhaite consulter et de se diriger vers les détails de celui-ci

.. image:: assets/images/details_archives_mobile.png