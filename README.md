# Archive manager documentation

Ceci est la documentation de Archive Manager en utilisant l'outils Sphinx
This is Archive Manager doc using Sphiinx and Readthedoc tool

## prerequisites

- Have at least version 3 of Python install
- Have virtual env creation tool such as virtualenv & pipenv
- For PDF generation, Latex have to be avalaible on your os

## Installation

### Creation of virtual environment on Linux with virtualenv tool

`virtualenv venv`<br>
`source venv/bin/activate`

### Project cloning

`git clone https://gitlab.com/tderick/archive-manager-documentation.git`

### Python dependencies installation

`cd archive-manager-documentation`<br>
`pip install -r requirements.txt`

### Creation of _static folder
`Creation of the _static folder in source folder`
#### 
`cd source`
####
`mkdir _static`

### HTML documentation generation

`make html`

### Title hierarchy on sphinx for this project

level 1: ================================

level 2: --------------------------------

level 3: ++++++++++++++++++++++++++++++++

level 4: ********************************

level 5: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

level 6: ################################

level 7: &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

level 8: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

level 9: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

level 10: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
